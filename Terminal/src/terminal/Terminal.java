package terminal;

import java.io.IOException;
import java.util.Scanner;

public class Terminal {
    public static void main(String[] args) throws IOException {
        
        Scanner t = new Scanner(System.in);
        boolean salida = true;
        while (salida==true){
            System.out.println("Ingresar comando: ");
            String com = t.nextLine();
        Scanner p = new Scanner (com);
        String aux=p.next();
           
        switch (aux){
            case "help":
                Comandos.help();
                break;
                
            case "cd":
                if (p.hasNext())
                    Comandos.cd(p.next());
                else 
                    Comandos.cd();
                break;
                
            case "dir":
                if (p.hasNext())
                    Comandos.dir(p.next());
                else
                Comandos.dir();
                break;
                
            case "close":
                salida=false;
                break;
                
            case "clear":
                Comandos.clear();
                break;
                
            case "mkdir":
                if (p.hasNext())
                Comandos.mkdir(p.next());
                else 
                    System.out.println("Ingresar comando Valido");
                break;
                
            case "info":
                if (p.hasNext())
                Comandos.info(p.next());
                else
                    System.out.println("Ingresar comando Valido");
                break;
                
            case "cat":
                if (p.hasNext())
                Comandos.cat(p.next());
                else
                    System.out.println("Ingresar comando Valido");
                break;
                
            case "top":
                if (p.hasNext()){
        try{
                int num = p.nextInt();
                Comandos.top(num, p.next());
        }catch(Exception e){
            System.out.println("Input Mismatch");
        }
                }else
                    System.out.println("Ingresar comando Valido");
                break;
                
            case "mkfile":
                if (p.hasNext()){
                 String j= p.next();
                    if (p.hasNext())
                      Comandos.mkfile(j, p.nextLine());
                    else
                        System.out.println("Ingresar comando Valido");
                }else
                    System.out.println("Ingresar comando Valido");
                
                break;
                
            case "write":
                if (p.hasNext()){
                    String j = p.next();
                      if (p.hasNext())
                            Comandos.wr(j,p.nextLine());
                      else 
                          System.out.println("Ingresar comando Valido");
                }else 
                    System.out.println("Ingresar comando Valido");
                break;
          
            case "delete":
                Comandos.delete(p.next());
                break;
            
            default:
                System.out.println("El comando ingresado no existe");
                
                
                
                
        
    }
    
}
    }
}

