package terminal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Comandos {
    
  static java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));
   
  
  public static void help(){
        System.out.println(" cd-> Muestra el directorio actual.\n" +
"           [..] -> Accede al directorio padre.\n" +
"           [<nombreDirectorio>] -> Accede a un directorio dentro del directorio actual.\n" +
"           [<rutaAbsoluta] -> Accede a la ruta absoluta del sistema.\n" +
" mkdir <nombre_directorio> -> Crea un directorio en la ruta actual.\n" +
" info <nombre> -> Muestra la información del elemento. Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace.\n" +
" cat <nombreFichero> -> Muestra el contenido de un fichero.\n" +
" top <numeroLineas> <nombreFichero> -> Muestra las líneas especificadas de un fichero.\n" +
" mkfile <nombreFichero> <texto> -> Crea un fichero con ese nombre y el contenido de texto.\n" +
" write <nombreFichero> <texto> -> Añade 'texto' al final del fichero especificado.\n" +
" dir -> Lista los archivos o directorios de la ruta actual.\n" +
"            [<nombreDirectorio>] -> Lista los archivos o directorios dentro de ese directorio.\n" +
"            [<rutaAbsoluta] -> Lista los archivos o directorios dentro de esa ruta.\n" +
" delete <nombre> -> Borra el fichero, si es un directorio borra todo su contenido y a si mismo.\n" +
" close -> Cierra el programa.\n" +
" Clear -> vacía la vista.");
    }
    
    
    public static void cd(){
        System.out.println(ruta);
    }
   
    
    public static void cd(String t){
        if (t.equals("..")){
                try{
            ruta=ruta.getParent();
            System.out.println(ruta);
                }catch(Exception e){
            System.out.println("El directorio no existe");
        }
    
        
        
        }else if (t.contains("\\")||t.contains("/")){
            try{
        File directorio = new File(t);
          if(directorio.exists()){
              java.nio.file.Path ruta = Paths.get(t);
              if (ruta.isAbsolute()){
                  System.out.println("La ruta es absoluta");
          }else
                  System.out.println("La ruta indicada no es absoluta");
          }else
                    System.out.println("La ruta indicada no existe");
        
        
        }   catch(Exception e){
            System.out.println("El la direccion no existe");
        }
        
        
        
        
        }else{
            try{
            java.nio.file.Path ruta1 = ruta.resolve(t);
            File directorio = new File(ruta1.toString());
            if (directorio.exists()){
            ruta=ruta1;
            System.out.println(ruta);
            }else
                System.out.println("El directorio no existe");
               
        }catch(Exception e){
            System.out.println("El directorio no existe");
        }
   
    }
    }
    
    
    public static void dir(){
        File directorio = new File(ruta.toString());
        if (directorio.exists()){
        File[] lista = directorio.listFiles();
        for (int i=0;i<lista.length;i++){
            System.out.println("          "+lista[i].getName());
        }
        }else
            System.out.println("El directorio no existe");
        
    }
    
    
    
    public static void dir(String e){
        if (e.contains("\\")||e.contains("/")){
            File r = new File(e);
            if (r.exists()){
                File[] lista = r.listFiles();
        for (int i=0;i<lista.length;i++){
            System.out.println("          "+lista[i].getName());
        }
            }else
                System.out.println("La ruta indicada no existe");
        
        
        } else{
            File r = new File(ruta.resolve(e).toString());
            if (r.exists() && r.isDirectory()){
                File[] lista = r.listFiles();
                if (lista.length==0){
                    System.out.println("El directorio esta vacio");
                }else
        for (int i=0;i<lista.length;i++){
            System.out.println("          "+lista[i].getName());
        }
            }else
                System.out.println("El directorio indicado no existe o no es un directorio");
            
        }
    }
    
    
    public static void clear(){
        for (int i=0;i<20;i++){
            System.out.println("");
        }
    }

    
    
    public static void mkdir(String q){
      File directorio = new File(q);
      directorio.mkdir();
        System.out.println("Directorio Creado Exitosamente");
    }
    
    
    public static void info(String r){
       try{ 
        File directorio = new File(r);
         if (directorio.exists()){
         if (directorio.isDirectory()){
        File[] lista = directorio.listFiles();
            System.out.println("File System: "+ruta.getFileSystem());
            System.out.println("Nombre: "+directorio.getName());
            System.out.println("Ruta: "+directorio.getAbsolutePath());
            System.out.println("Espacio Libre: "+directorio.getFreeSpace()+" bytes");
            System.out.println("Espacio total: "+directorio.getTotalSpace()+" bytes");
            System.out.println("Espacio Usable: "+directorio.getUsableSpace()+" bytes");
            System.out.println("Numero de Elementos: "+lista.length);
            System.out.println("Parent: "+directorio.getParent());
            
        
        
        
        }else{
            File[] lista = directorio.listFiles();
            if (directorio.exists()){
            System.out.println("File System: "+ruta.getFileSystem());
            System.out.println("Nombre: "+directorio.getName());
            System.out.println("Ruta: "+directorio.getAbsolutePath());
            System.out.println("Espacio Libre: "+directorio.getFreeSpace()+" bytes");
            System.out.println("Espacio total: "+directorio.getTotalSpace()+" bytes");
            System.out.println("Espacio Usable: "+directorio.getUsableSpace()+" bytes");
            System.out.println("Parent: "+directorio.getParent());
            }
        }
        }else
            System.out.println("El directorio no existe");
       }catch(Exception e){
            System.out.println("El directorio no existe o el elemento no es un directorio");
        }
    }
    
    public static void cat(String q) throws FileNotFoundException, IOException{
        try{
          BufferedReader br = new BufferedReader(new FileReader(q));
            String linea, linea2 = new String();
            
            while((linea=br.readLine()) != null ){
                linea2 = linea2 + linea + "\n";
                
            }
            System.out.println(linea2);
            br.close();
        
    }catch(Exception e){
            System.out.println("El Fichero no existe");
        } 
                            
}
    
    public static void top(int num, String nom) throws IOException{
        try{
            
        String linea = Files.readAllLines(Paths.get(nom)).get(num);
        System.out.println(linea); 
        
        }catch(Exception e){
            System.out.println("El Fichero no existe o el número supera la longitud del archivo");
        } 
        
    }
    
    public static void mkfile(String nom,String tx) throws IOException{
        try{
        if (nom.length()>=4&&nom.substring(nom.length()-4).equals(".txt")){
        File fichero = new File(nom);
        
        if (!fichero.exists()){
        
        BufferedWriter bw = new BufferedWriter(new FileWriter(nom));
        PrintWriter salida = new PrintWriter(bw);
        salida.println(tx);
        bw.close();
        salida.close();
            System.out.println("Fichero creado y escrito con Exito");
    }else
            System.out.println("Este fichero ya existe");
    }else
            System.out.println("Nombre de fichero invalido");
        }catch(Exception e){
            System.out.println("Nombre de fichero invalido");
}
}
    
    public static void wr(String nom, String tx) throws IOException{
        File fichero = new File(nom);
         
        if (fichero.exists()){
            FileWriter wr=new FileWriter(nom,true);
            BufferedWriter bw = new BufferedWriter(wr);
            bw.write(tx);
            bw.flush();
            bw.close();
            wr.close();
            System.out.println("Texto Añadido");
        }else
            System.out.println("El fichero no existe");
            
    }
    
    public static void delete(String nom){
        File directorio = new File(nom);
        if (directorio.exists()){
            directorio.delete();
            System.out.println("Archivo Borrado");
        }else
            System.out.println("El directorio/archivo no existe");
    }
}
